# Перечень
| Наименование | Описание |
| ------ | ------ |
| http://80.79.246.112:8080/Trader | Демо ссылка |
| https://gitlab.com/DevE-ng/moex-dev-test/blob/master/Pack.zip | TradeSystem.ear, log |  

# Запуск приложения для отладки возможно с помощью двух способов

## Запуск приложение под docker
* установить  Docker http://www.docker.com
* запустить docker/docker-compose up
* запустить docker exec -it jboss bash и затем 

<code>bash jboss-cli.sh 
connect 
/subsystem=web/connector=http/:write-attribute(name=protocol,value=org.apache.coyote.http11.Http11NioProtocol) 
:reload 
</code> 
* запустить mvn package в корне проекта

## Локально
* поставить Jboss
* создать базу 127.0.0.1, порт 3050. название базы trade_sys_db пользователь TSDA пароль TraDeSystem2019

* включить поддержку сокетов в Jboss с помощью следующих команд в cli jboss: 

<code>bash jboss-cli.sh 
connect 
/subsystem=web/connector=http/:write-attribute(name=protocol,value=org.apache.coyote.http11.Http11NioProtocol) 
:reload
</code> 
* прописать в хостах связь 127.0.0.1 firebird
* установить \docker\standalone\deployments\TradeSystem.ear в jboss


Для загрузки списка продуктов используем /Trader/import.jsp
