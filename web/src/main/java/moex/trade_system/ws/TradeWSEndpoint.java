package moex.trade_system.ws;


import com.google.gson.Gson;
import moex.trade_system.db.DealDB;
import moex.trade_system.db.WSObject;
import org.apache.log4j.Logger;

import javax.ejb.Singleton;
import javax.websocket.*;

import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Singleton
@ServerEndpoint("/tradeupdate")
public class TradeWSEndpoint {
    private Logger logger = Logger.getLogger(getClass());
    private static Set<Session> clients = Collections.synchronizedSet(new HashSet<Session>());


    @OnOpen
    public void onConnect(Session session) throws IOException {
        logger.debug("WebSocket opened: " + session.getId());
        clients.add(session);
    }


    public void sendToAll(WSObject wsObject) {
        for (Session client : clients) {
            try {
                client.getBasicRemote().sendText(wsObject.jsonData());
            } catch (Exception e) {
                clients.remove(client);
            }
        }
    }


    @OnClose
    public void onClose(Session session) {
        clients.remove(session);
    }

    @OnError
    public void onError(Session session, Throwable t) {

        logger.error("Error occured {0}", t);


    }
}


