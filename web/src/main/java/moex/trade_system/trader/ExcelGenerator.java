package moex.trade_system.trader;

import org.apache.poi.hssf.usermodel.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

public class ExcelGenerator {
    private HSSFWorkbook workbook = new HSSFWorkbook();
    private HSSFSheet sheet;
    int rowIndex = 0;
    int cellIndex = 0;
    private HSSFRow row;
    private HSSFCell cell;

    public void start() {
        sheet = workbook.createSheet("Отчет");

    }

    public void addRow(List<String> rowItems) {
        nextRow();
        for (String h : rowItems)
            cellValue(h);


    }

    private void nextCell() {
        cell = row.createCell(cellIndex);
        cellIndex++;
    }

    public int getCellIndex() {
        return cellIndex;
    }

    public ExcelGenerator cellValue(String value) {
        cell.setCellValue(value);
        nextCell();
        return this;
    }

    public ExcelGenerator cellValue(Float value) {
        cell.setCellValue(value);
        nextCell();
        return this;
    }


    public ExcelGenerator cellValue(Integer value) {
        cell.setCellValue(value);
        nextCell();
        return this;
    }

    public ExcelGenerator cellValue(Long value) {
        cell.setCellValue(value);
        nextCell();
        return this;
    }

    public ExcelGenerator nextRow() {
        cellIndex = 0;
        row = sheet.createRow((short) rowIndex);
        rowIndex++;
        nextCell();
        return this;
    }

    public void finish(HttpServletResponse response, String reportName) throws IOException {
        HSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        workbook.write(bos);
        response.setContentLength(bos.size());
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename=" + reportName + ".xls");
        workbook.write(response.getOutputStream());


        workbook.close();
    }
}
