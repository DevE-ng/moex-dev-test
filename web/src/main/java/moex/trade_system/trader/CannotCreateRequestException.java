package moex.trade_system.trader;

public class CannotCreateRequestException extends Exception {
    public CannotCreateRequestException() {
    }

    public CannotCreateRequestException(String message) {
        super(message);
    }

    public CannotCreateRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public CannotCreateRequestException(Throwable cause) {
        super(cause);
    }

    public CannotCreateRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
