package moex.trade_system.db;


import javax.persistence.*;

@Entity
@Table(name = "PRODUCTS")
public class ProductDB {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    private String productName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }


}
