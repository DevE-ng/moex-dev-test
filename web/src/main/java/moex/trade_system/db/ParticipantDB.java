package moex.trade_system.db;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "participants")
public class ParticipantDB {
    @Id
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String sessionId) {
        this.userId = sessionId;
    }

    private Timestamp createdAt;

    @Column(name="created_at")
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp date) {
        this.createdAt = date;
    }
}
