package moex.trade_system.db.enums;

public enum RequestState {
    ACTIVE, COMPLETED
}
