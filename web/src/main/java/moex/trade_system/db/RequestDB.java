package moex.trade_system.db;

import moex.trade_system.db.enums.RequestDirection;
import moex.trade_system.db.enums.RequestState;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "REQUESTS")
public class RequestDB  extends BaseWSObject {
    @Id
    private Long id;

    @Transient
    private WSKind kind=WSKind.REQUEST;



    private RequestDirection direction;

    @Column(name = "direction", nullable = false)
    @Enumerated(EnumType.STRING)
    public RequestDirection getDirection() {
        return direction;
    }

    public void setDirection(RequestDirection direction) {
        this.direction = direction;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id")
    public ProductDB product;


    public ProductDB getProduct() {
        return product;
    }

    public void setProduct(ProductDB product) {
        this.product = product;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "participant_id", nullable = false)
    private ParticipantDB participant;

    public ParticipantDB getParticipant() {
        return participant;
    }

    public void setParticipant(ParticipantDB participant) {
        this.participant = participant;
    }


    private RequestState state;

    @Enumerated(EnumType.STRING)
    public RequestState getState() {
        return state;
    }

    public void setState(RequestState state) {
        this.state = state;
    }

    private Timestamp createdAt;

    @Column(name = "created_at")
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp date) {
        this.createdAt = date;
    }

    private Float price;
    private Integer amount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "#" + id + " (" + direction + ", " + state + " " + price + ")";
    }

}
