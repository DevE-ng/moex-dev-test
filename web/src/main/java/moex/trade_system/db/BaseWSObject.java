package moex.trade_system.db;

import com.google.gson.Gson;

public abstract class BaseWSObject implements WSObject{


    @Override
    public String jsonData() {
        return new Gson().toJson(this);
    }
}
