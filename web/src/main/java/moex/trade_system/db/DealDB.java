package moex.trade_system.db;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "deals")
public class DealDB extends BaseWSObject {
    @Id
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private Timestamp createdAt;

    @Column(name = "created_at")
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp date) {
        this.createdAt = date;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "buyer_request_id")
    private RequestDB buyerRequest;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "seller_request_id")
    private RequestDB sellerRequest;

    public RequestDB getBuyerRequest() {
        return buyerRequest;
    }

    public void setBuyerRequest(RequestDB buyerRequest) {
        this.buyerRequest = buyerRequest;
    }

    public RequestDB getSellerRequest() {
        return sellerRequest;
    }

    public void setSellerRequest(RequestDB sellerRequest) {
        this.sellerRequest = sellerRequest;
    }

    private Integer amount;
    private Float price;


    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    @Transient
    private WSKind kind=WSKind.DEAL;



}
