package moex.trade_system.dao;

import moex.trade_system.db.DealDB;
import moex.trade_system.db.ParticipantDB;
import moex.trade_system.db.ProductDB;
import moex.trade_system.db.RequestDB;
import moex.trade_system.db.enums.RequestDirection;
import moex.trade_system.db.enums.RequestState;
import moex.trade_system.http.payloads.RequestPayload;
import moex.trade_system.trader.CannotCreateRequestException;
import moex.trade_system.ws.TradeWSEndpoint;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static javax.management.Query.eq;

@Stateless
public class TraderDAO {

    @PersistenceContext
    EntityManager em;


    private Logger logger = Logger.getLogger(getClass());

    @TransactionAttribute
    public void addProducts(Map<Long, String> products) {
        for (Map.Entry<Long, String> pair : products.entrySet()) {
            logger.info("Add product to database " + pair.getKey() + "." + pair.getValue());
            ProductDB productDB = new ProductDB();
            productDB.setProductName(pair.getValue());
            productDB.setId(pair.getKey());
            em.merge(productDB);
        }

    }

    @TransactionAttribute
    private ParticipantDB getOrCreateParticipant(String userId) {
        ParticipantDB participant = null;
        try {
            participant = (ParticipantDB)
                    em.createQuery("select p from ParticipantDB p where p.userId=:userId").setParameter("userId", userId).getSingleResult();
        } catch (NoResultException exception) {
            logger.error("since there is no registration procedure so we create new participant on fly");
            Date date = new Date();
            participant = new ParticipantDB();
            participant.setUserId(userId);
            participant.setId(date.getTime());
            participant.setCreatedAt(new Timestamp(date.getTime()));
            em.persist(participant);
        }

        return participant;
    }

    @EJB
    TradeWSEndpoint ws;

    @TransactionAttribute
    synchronized public void solveRequest(RequestPayload requestPayload) throws CannotCreateRequestException, IOException {
        logger.info("strart register new request:" + requestPayload);
        ParticipantDB participant = getOrCreateParticipant(requestPayload.getUserId());
        ProductDB product = em.find(ProductDB.class, requestPayload.getProduct());

        if (requestPayload.getTotal() > 1000000) //TODO should be in config
            throw new CannotCreateRequestException("Request should be less than 1000000");//TODO should be like anchor

        RequestDB request = new RequestDB();
        Date date = new Date();
        request.setAmount(requestPayload.getAmount());
        request.setParticipant(participant);
        request.setState(RequestState.ACTIVE);
        request.setPrice(requestPayload.getPrice());
        request.setDirection(requestPayload.getDirection());
        request.setProduct(product);
        request.setCreatedAt(new Timestamp(date.getTime()));

        request.setId(date.getTime()); //TODO should be sequence generator. problems with configuring

        em.persist(request);
        logger.info("request stored to db:" + requestPayload + ", start check deal ability");


        DealDB deal = processDeal(request);

        if (deal != null) {

            ws.sendToAll(deal);
        } else
            ws.sendToAll(request);

    }

    private List<RequestDB> foundRelativeSell(RequestDB request) {
        return em.createQuery("select r from RequestDB r where " +
                "r.state=moex.trade_system.db.enums.RequestState.ACTIVE and " +
                "r.direction= moex.trade_system.db.enums.RequestDirection.BUY and " +
                "r.price >= :price and " +
                "r.product=:product " +
                "order by r.id desc")
                .setParameter("price", request.getPrice())
                .setParameter("product", request.getProduct())
                .setMaxResults(1)
                .getResultList();
    }

    private List<RequestDB> foundRelativeBuy(RequestDB request) {
        return em.createQuery("select r from RequestDB r where " +
                "r.state=moex.trade_system.db.enums.RequestState.ACTIVE and " +
                "r.direction= moex.trade_system.db.enums.RequestDirection.SELL and " +
                "r.price <= :price and " +
                "r.product=:product " +
                "order by r.id desc")
                .setParameter("price", request.getPrice())
                .setParameter("product", request.getProduct())
                .setMaxResults(1)
                .getResultList();
    }

    public DealDB processDeal(RequestDB request) {
        logger.info("Check can deal for request #" + request);

        List<RequestDB> relatives = request.getDirection().equals(RequestDirection.SELL) ?
                foundRelativeSell(request) :
                foundRelativeBuy(request);
        if (!relatives.isEmpty()) {
            RequestDB relative = relatives.get(0);

            logger.info("Create deal between #" + request + " and " + relative);
            DealDB deal = new DealDB();
            Date date = new Date();

            boolean isSell = request.getDirection().equals(RequestDirection.SELL);
            RequestDB sellerRequest = isSell ? request : relative;
            RequestDB buyerRequest = isSell ? relative : request;
            deal.setSellerRequest(sellerRequest);
            deal.setBuyerRequest(buyerRequest);

            deal.setAmount(Math.min(relative.getAmount(), request.getAmount()));
            deal.setPrice(request.getPrice());

            deal.setId(date.getTime());
            deal.setCreatedAt(new Timestamp(date.getTime()));
            em.persist(deal);
            relative.setState(RequestState.COMPLETED);
            request.setState(RequestState.COMPLETED);
            em.merge(relative);
            em.merge(request);
            logger.info("Deal #" + deal.getId() + " saved to db");
            logger.info("Request " + relative + " Completed");
            logger.info("Request " + request + " Completed");
            return deal;
        }
        return null;

    }


}
