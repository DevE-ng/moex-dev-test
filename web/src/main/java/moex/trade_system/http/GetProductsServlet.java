package moex.trade_system.http;

import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/trade/products")
public class GetProductsServlet  extends BaseServlet {

    @PersistenceContext
    EntityManager em;

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        gson(resp,em.createQuery("select p from ProductDB p order by  p.productName").getResultList());
    }
}
