package moex.trade_system.http;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/trade/deals")
public class GetDealsServlet extends BaseServlet {
    @PersistenceContext
    EntityManager em;

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws  IOException {
        gson(resp, em.createQuery("select p from DealDB p order by  p.id desc").getResultList());
    }
}
