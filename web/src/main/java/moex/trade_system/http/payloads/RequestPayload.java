package moex.trade_system.http.payloads;

import moex.trade_system.db.enums.RequestDirection;

public class RequestPayload {
    private Long product;
    private RequestDirection direction;
    private Integer amount = 0;
    private Float price = 0f;
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getProduct() {
        return product;
    }

    public void setProduct(Long product) {
        this.product = product;
    }

    public RequestDirection getDirection() {
        return direction;
    }

    public void setDirection(RequestDirection direction) {
        this.direction = direction;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return direction + ", product #" + product + " $" + price + " * " + amount;
    }

    public Float getTotal() {
        return price * amount;
    }
}
