package moex.trade_system.http;

import com.google.gson.Gson;
import moex.trade_system.db.ProductDB;
import moex.trade_system.db.RequestDB;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/trade/requests")
public class GetRequestsServlet extends BaseServlet {
    @PersistenceContext
    EntityManager em;

    private Logger logger = Logger.getLogger(getClass());

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        gson(resp,
                em.createQuery("select p from RequestDB p " +
                "where p.state=moex.trade_system.db.enums.RequestState.ACTIVE " +
                "and p.amount>0 " +
                "and p.price>0 order by  p.id desc")
                .getResultList());
    }
}
