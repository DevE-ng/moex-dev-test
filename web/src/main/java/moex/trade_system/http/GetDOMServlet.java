package moex.trade_system.http;

import com.google.gson.Gson;
import moex.trade_system.db.ProductDB;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/trade/dom")
public class GetDOMServlet extends BaseServlet {
    @PersistenceContext
    EntityManager em;

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Long productId = Long.parseLong(req.getParameter("productId"));

        gson(resp, em.createQuery("select p from RequestDB p where " +
                "p.state=moex.trade_system.db.enums.RequestState.ACTIVE " +
                "and p.amount>0 and p.price>0 and  p.product.id=:productId ").setParameter("productId", productId).getResultList());

    }
}
