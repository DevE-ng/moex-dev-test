package moex.trade_system.http;


import com.google.gson.Gson;
import moex.trade_system.dao.TraderDAO;
import moex.trade_system.http.payloads.RequestPayload;
import moex.trade_system.trader.CannotCreateRequestException;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/trade/add_request")
public class AddRequestServlet extends HttpServlet {


    @EJB
    TraderDAO tradeManager;

    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        RequestPayload requestPayload = new Gson().fromJson(req.getReader(), RequestPayload.class);
        try {
            tradeManager.solveRequest(requestPayload);

        } catch (CannotCreateRequestException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
