package moex.trade_system.http.reports;

import moex.trade_system.db.DealDB;
import moex.trade_system.db.ProductDB;
import moex.trade_system.db.RequestDB;
import moex.trade_system.db.enums.RequestDirection;
import moex.trade_system.http.BaseServlet;
import moex.trade_system.trader.ExcelGenerator;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebServlet("/trade/requests/excel")
public class GetExcelRequestsServlet extends BaseServlet {
    @PersistenceContext
    EntityManager em;

    private Logger logger = Logger.getLogger(getClass());

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<RequestDB> requests = em.createQuery("select p from RequestDB p " +
                "where p.state=moex.trade_system.db.enums.RequestState.ACTIVE " +
                "and p.amount>0 " +
                "and p.price>0 order by  p.id desc")
                .getResultList();

        ExcelGenerator eg = new ExcelGenerator();

        eg.start();
        eg.addRow(Arrays.asList("Дата время", "Товар", "Направление", "Количество", "Цена", "Разместил"));

        for (RequestDB request : requests) {
            eg.nextRow()
                    .cellValue(request.getCreatedAt().toString())
                    .cellValue(request.getDirection().equals(RequestDirection.SELL) ? "Продажа" : "Покупка")
                    .cellValue(request.getAmount())
                    .cellValue(request.getPrice())
                    .cellValue(request.getParticipant().getUserId());

        }

        eg.finish(resp, "requests");
    }
}
