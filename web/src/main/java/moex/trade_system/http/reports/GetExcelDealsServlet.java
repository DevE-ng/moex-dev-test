package moex.trade_system.http.reports;

import com.google.gson.Gson;
import moex.trade_system.db.DealDB;
import moex.trade_system.db.ProductDB;
import moex.trade_system.trader.ExcelGenerator;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebServlet("/trade/deals/excel")
public class GetExcelDealsServlet extends HttpServlet {
    @PersistenceContext
    EntityManager em;

    private Logger logger = Logger.getLogger(getClass());

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        List<DealDB> deals = em.createQuery("select p from DealDB p order by  p.id desc").getResultList();

        ExcelGenerator eg = new ExcelGenerator();

        eg.start();
        eg.addRow(Arrays.asList("Дата время", "Товар", "Продавец", "Покупатель", "Цена", "Количество"));


        for (DealDB deal : deals) {
            eg.nextRow()
                    .cellValue(deal.getCreatedAt().toString())
                    .cellValue(deal.getBuyerRequest().getProduct().getProductName())
                    .cellValue(deal.getSellerRequest().getParticipant().getUserId())
                    .cellValue(deal.getBuyerRequest().getParticipant().getUserId())
                    .cellValue(deal.getPrice())
                    .cellValue(deal.getAmount());
        }

        eg.finish(resp, "deals");
    }
}
