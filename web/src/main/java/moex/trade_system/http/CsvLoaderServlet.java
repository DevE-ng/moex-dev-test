package moex.trade_system.http;

import moex.trade_system.dao.TraderDAO;
import moex.trade_system.ws.TradeWSEndpoint;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


@WebServlet("/trade/load_csv_products")
@MultipartConfig(fileSizeThreshold = 1024 * 1024,
        maxFileSize = 1024 * 1024 * 5,
        maxRequestSize = 1024 * 1024 * 5 * 5)
public class CsvLoaderServlet extends HttpServlet {


    @EJB
    TraderDAO dao;



    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">

        BufferedReader br = new BufferedReader(new InputStreamReader(filePart.getInputStream()));
        String line;
        Map<Long, String> products = new HashMap<>();
        while ((line = br.readLine()) != null) {
            String[] row = line.split(",");
            if (row.length == 2) {

                products.put(Long.parseLong(row[0]), row[1]);
            }

        }
        dao.addProducts(products);
        response.sendRedirect("/Trader");

    }

}
