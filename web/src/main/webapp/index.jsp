<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="stylesheet" href="css/bootstrap-grid.min.css"/>
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/styles.css"/>
    <title>ТКС УРОЖАЙ</title>
</head>
<body>
<div id="app">
    <div class="main-container">
        <div class="container-row">
            <div class="col">
                Текущий пользователь: {{newRequest.userId}}
                <a href="import.jsp">Загрузить CSV продукты</a>
                <div class="row">
                    <div class="col-md-4">
                        <div class="border border-dark new-request">
                            <h2>Новая заявка</h2>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="directionRequest" class="col-form-label">Направление</label>
                                </div>
                                <div class="col-sm-9"><select class="form-control" v-model="newRequest.direction"
                                                              id="directionRequest"
                                                              @change="validate">
                                    <option value="BUY">Покупка</option>
                                    <option value="SELL">Продажа</option>

                                </select></div>
                            </div>

                            <div class="form-group row">
                                <label for="productRequest" class="col-sm-3 col-form-label">Товар</label>
                                <div class="col-sm-9"><select class="form-control" v-model="newRequest.product"
                                                              id="productRequest"
                                                              @change="validate">
                                    <option v-for="item in products" :value="item.id">{{item.productName}}</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="countRequest" class="col-sm-3 col-form-label">Количество</label>
                                <div class="col-sm-9"><input @change="validate" class="form-control" type="text"
                                                             v-model="newRequest.amount"
                                                             id="countRequest"
                                                             placeholder="Введите количество">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="costRequest" class="col-sm-3 col-form-label">Цена</label>
                                <div class="col-sm-9"><input @change="validate" class="form-control" type="text"
                                                             v-model="newRequest.price"
                                                             id="costRequest"
                                                             placeholder="Введите цену">
                                </div>
                            </div>

                            <span class="red">{{errorBox}}</span>

                            <button @click="registerRequest" class="btn btn-primary">Зарегистрировать</button>


                        </div>
                    </div>
                    <div class="col-md-8 dom-block">

                        <div class="row text-center">

                            <div class="col"><h2>Биржевой стакан</h2></div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 col-right">
                                <div class="form-group row">
                                    <label class="col-form-label" for="productRequest1">Товар</label>
                                    <div class="col"><select @change="changeProductDOM" class="form-control"
                                                             v-model="domSelector"
                                                             id="productRequest1">
                                        <option v-for="item in products" :value="item.id">{{item.productName}}</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-container">
                            <div class="dom-table-block">
                                <table>
                                    <thead>
                                    <tr>
                                        <th>Дата время
                                            <div>Дата время</div>
                                        </th>
                                        <th>Товар
                                            <div>Товар</div>
                                        </th>
                                        <th>Направление
                                            <div>Направление</div>
                                        </th>
                                        <th>Цена
                                            <div>Цена</div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="item in dom" :class="'dir'+item.direction">
                                        <td>{{item.createdAt}}</td>
                                        <td>{{item.product.productName}}</td>
                                        <td>{{item.direction|formatDirection}}</td>
                                        <td>{{item.price}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-row">
            <div class="list row">
                <div class="col">
                    <h2>Список заявок</h2>
                </div>
                <a href="trade/requests/excel"><img width="32px" height="32px"
                                                    src="images/download.png"
                                                    class="load">Выгрузить список в EXCEL</a>
            </div>
            <div class="table-container">
                <div class="requests-block table-block">
                    <table>
                        <thead>
                        <tr>
                            <th>Дата время
                                <div>Дата время</div>
                            </th>
                            <th>Товар
                                <div>Товар</div>
                            </th>
                            <th>Направление
                                <div>Направление</div>
                            </th>

                            <th>Количество
                                <div>Количество</div>
                            </th>
                            <th>Цена
                                <div>Цена</div>
                            </th>
                            <th>Разместил
                                <div>Разместил</div>
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr v-for="item in requests" :class="'dir'+item.direction">
                            <td>№{{item.id}}<br>{{item.createdAt}}</td>
                            <td>{{item.product.productName}}</td>
                            <td>{{item.direction|formatDirection}}</td>
                            <td>{{item.amount}}</td>
                            <td>{{item.price}}</td>
                            <td>Пользователь №{{item.participant.userId}}</td>
                        </tr>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>

        <div class="container-row">
            <div class="list row">
                <div class="col">
                    <h2>Список сделок</h2>
                </div>

                <a href="trade/deals/excel"><img width="32px" height="32px"
                                                 src="images/download.png"
                                                 class="load">Выгрузить список в EXCEL</a>
            </div>
            <div class="table-container">
                <div class="deals-block table-block">
                    <table>
                        <thead>
                        <tr>
                            <th>Дата время
                                <div>Дата время</div>
                            </th>
                            <th>Товар
                                <div>Товар</div>
                            </th>
                            <th>Продавец
                                <div>Продавец</div>
                            </th>
                            <th>Покупатель
                                <div>Покупатель</div>
                            </th>
                            <th>Цена
                                <div>Цена</div>
                            </th>
                            <th>Количество
                                <div>Количество</div>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="item in deals">
                            <td>№{{item.id}}<br>{{item.createdAt}}</td>
                            <td>{{item.buyerRequest.product.productName}}</td>
                            <td>Пользователь №{{item.sellerRequest.participant.userId}}</td>
                            <td>Пользователь №{{item.buyerRequest.participant.userId}}</td>
                            <td>{{item.price}}</td>
                            <td>{{item.amount}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>