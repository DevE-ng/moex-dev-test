function isInt(n) {
    return n % 1 === 0;
}

function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

Vue.filter('formatDirection', function (value) {
    if (value) {
        return value === 'BUY' ? 'Покупка' : 'Продажа';
    }
});
var app = new Vue({
    el: '#app',
    data: function () {
        return {
            newRequest: {amount: 10, price: 10, direction: "BUY", product: null, userId: null},
            domProduct: null,
            products: [],
            errorBox: "",
            requests: [],
            deals: [],
            dom: [],
            connectCheckInterval: null,

            domSelector: null,
        }
    },
    created: function () {
        this.newRequest.userId = getCookie("userId");
        if (!this.userId) {
            this.newRequest.userId = new Date().getTime();
            setCookie("userId", this.newRequest.userId);
        }

        this.connectToWs();
        this.loadProducts();


    },
    methods: {

        validate: function () {
            this.errorBox = "";
            if (!this.newRequest.amount || !isInt(this.newRequest.amount) || this.newRequest.amount <= 0) {
                this.errorBox = "Необходимо ввести количество";
                return false;
            }
            if (!this.newRequest.price || !isInt(this.newRequest.price) || this.newRequest.price <= 0) {
                this.errorBox = "Необходимо ввести цену больше нуля";
                return false;
            }
            if (!this.newRequest.direction) {
                this.errorBox = "Необходимо выбрать направление";
                return false;
            }
            if (!this.newRequest.product) {
                this.errorBox = "Необходимо выбрать продукт";
                return false;
            }
            var totalSum = this.newRequest.amount * this.newRequest.price;

            if (totalSum <= 0) {
                this.errorBox = "Общая сумма заявки должна быть больше нуля";
                return false;
            }

            if (totalSum > 1000000) {
                this.errorBox = "Общая сумма заявки не может превышать 1 000 000";
                return false;
            }
            return true;
        },

        registerRequest: function () {

            console.log("Register new request", this.newRequest);

            if (this.validate()) {
                var vm = this;
                axios
                    .put('trade/add_request', this.newRequest)
                    .then(
                        function (data) {

                        });
            }

        },
        loadProducts: function () {
            var vm = this;
            axios
                .get('trade/products')
                .then(
                    function (data) {
                        vm.products = data.data;
                        if (vm.products.length > 0) {
                            var id = vm.products[0].id;
                            console.log("set default poroduct" + id);
                            vm.domSelector = id;
                            vm.newRequest.product = id;
                            vm.reload();
                        }

                    });
        },

        connectToWs: function () {
            this.loadProducts();
            var waUrl = ((window.location.protocol === "https:") ? "wss://" : "ws://") + window.location.host + "/Trader/tradeupdate";
            console.log("connectToWs:" + waUrl)
            var webSocket = new WebSocket(waUrl);
            var vm = this;
            if (vm.connectCheckInterval) {
                clearInterval(vm.connectCheckInterval);
                vm.connectCheckInterval = null;
            }



            webSocket.onmessage = function (message) {
                console.log("Message", message);
                vm.socketEvent(message);


            };
            webSocket.onopen = function () {
                console.log("connection opened");
            };

            webSocket.onclose = function () {
                console.log("connection closed");
                vm.connectCheckInterval = setInterval(function () {
                  //  vm.connectToWs();
                }, 1000)
            };

            webSocket.onerror = function wserror(message) {
                console.log("error: " + message);
            }

        },
        socketEvent: function (message) {

            if (message.data) {
                var data = JSON.parse(message.data);
                console.log("kind", data);
                if (data.kind === 'DEAL') {
                    this.requests = this.requests.filter(function (item) {
                        return data.buyerRequest.id !== item.id && data.sellerRequest.id !== item.id;
                    });
                    this.deals.push(data);
                    this.sortDeals();
                } else
                    this.requests.push(data);

                console.log("Requests", this.requests);

                this.sortRequests();
            }
        },
        sortDeals: function () {
            this.deals = this.deals.sort(function (a, b) {
                return (parseInt(b.id) - parseInt(a.id));
            });
        },
        sortRequests: function () {
            this.requests = this.requests.sort(function (a, b) {
                return (parseInt(b.id) - parseInt(a.id));
            });
            this.loadDom();
        },

        loadRequests: function () {

            var vm = this;
            axios
                .get('trade/requests')
                .then(
                    function (data) {
                        vm.requests = data.data;
                        vm.sortRequests();
                        vm.loadDom();
                    });
        },
        loadDom: function () {
            var vm = this;
            this.dom = this.requests.filter(function (r) {
                return r.product.id == vm.domSelector;
            }).sort(function (a, b) {

                var compareByDirection = a.direction.localeCompare(b.direction);

                if (compareByDirection !== 0)
                    return compareByDirection;

                var compareByPrice = (parseFloat(b.price) - parseFloat(a.price));
                if (compareByPrice !== 0)
                    return compareByPrice;

                var compareById = (parseInt(a.id) - parseInt(b.id));
                return (a.direction === 'SELL' ? -1 : 1) * compareById;

            });
        },
        loadDeals: function () {

            var vm = this;
            axios
                .get('trade/deals')
                .then(
                    function (data) {
                        vm.deals = data.data;
                        vm.sortDeals();
                    });
        },
        reload: function () {
            this.loadDeals();
            this.loadRequests();
        },
        changeProductDOM: function (item) {
            console.log("change dom " + this.domSelector);
            this.loadDom();
        }
    }
})
